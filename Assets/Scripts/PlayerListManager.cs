﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerListManager : MonoBehaviour
{
    private Text playerList;
    private List<string> playerListBuffer = new List<string>();

    private static PlayerListManager instance;
    public static PlayerListManager InstancePlayerListManager() { return instance; }

    public enum ListEvent { build, add, remove }
    private void Awake()
    {
        instance = this;
        playerList = GetComponent<Text>();
    }
    public void RefreshPlayerList(ListEvent listEvent ,string playerName)
    {
        switch (listEvent) 
        {
            case ListEvent.build:
                int.TryParse(playerName.Split(':')[1], out int playerCount);
                for (int i = 0; i < playerCount; i++)
                {
                    playerListBuffer.Add(playerName.Split(':')[i + 2]);
                }
                break;
            case ListEvent.add:
                playerListBuffer.Add(playerName);
                break;
            case ListEvent.remove:
                playerListBuffer.Remove(playerName);
                break;
            default:
                break;
        }
        playerListBuffer.Sort();
        playerList.text = "";
        foreach (string player in playerListBuffer)
        {
            playerList.text += $" {player}\n";
        }
    }
}
