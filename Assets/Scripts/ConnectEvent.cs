﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ConnectEvent : MonoBehaviour
{
    public InputField iP;
    public InputField port;
    public InputField clientName;
    public ClientManager clientManager;

    private void Awake()
    {
        iP.text = "127.0.0.1";
        port.text = "4099";
    }
    public void OnConnect()
    {
        int.TryParse(port.text, out int portInt);
        if (clientManager.Connect(iP.text, portInt, clientName.text))
        {
            EventSystem.current.SetSelectedGameObject(null);
            gameObject.SetActive(false);
        }
    }
}
