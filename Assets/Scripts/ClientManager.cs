﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using UnityEngine;

public class ClientManager : MonoBehaviour
{
    private TcpClient client = new TcpClient();
    private CommandManager commandManager;
    private void Awake()
    {
        commandManager = new CommandManager();
    }
    private void Start()
    {
        InvokeRepeating(nameof(ClientUpdate), 0, 0.1f);
    }
    private void ClientUpdate()
    {
        if (client.Available <= 0)
        {
            return;
        }
        else
        {
            int iNumBytes = client.Available;
            byte[] buffer = new byte[iNumBytes];
            client.GetStream().Read(buffer, 0, iNumBytes);
            string request = Encoding.UTF8.GetString(buffer);
            commandManager.ReceivedCommands(request);
        }
    }
    public bool Connect(string address, int port, string clientName)
    {
        try
        {
            IPHostEntry host = Dns.GetHostEntry(address);
            IPAddress ipAddress = null;
            foreach (IPAddress h in host.AddressList)
            {
                if (h.AddressFamily == AddressFamily.InterNetwork)
                {
                    ipAddress = h;
                    break;
                }
            }
            client.Connect(ipAddress.ToString(), port);
            string request = "Name:" + clientName;
            byte[] requestBuffer = Encoding.UTF8.GetBytes(request);
            client.GetStream().Write(requestBuffer, 0, requestBuffer.Length);
            Debug.Log($" Chatroom {address}:{port} connected.");
            return true;
        }
        catch (Exception e)
        {
            Debug.LogError($"Exception happened: {e}");
            return false;
        }
    }
    public void SendMessages(string message)
    {
        string request = "Send:" + message;
        byte[] requestBuffer = Encoding.UTF8.GetBytes(request);
        client.GetStream().Write(requestBuffer, 0, requestBuffer.Length);
    }
}
