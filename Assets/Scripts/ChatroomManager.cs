﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class ChatroomManager : MonoBehaviour
{
    private Text chatroom;

    private static ChatroomManager instance;
    public static ChatroomManager InstanceChatroomManager() { return instance; }
    public enum ChatroomEvent { connect, message, disconnect }
    private void Awake()
    {
        instance = this;
        chatroom = GetComponent<Text>();
    }
    public void RefreshChatroom(ChatroomEvent chatroomEvent, string client, string message = "")
    {
        switch (chatroomEvent)
        {
            case ChatroomEvent.connect:
                chatroom.text += $" [{DateTime.Now:HH:mm:ss}] {client} just joined.\n";
                break;
            case ChatroomEvent.message:
                chatroom.text += $" [{DateTime.Now:HH:mm:ss}] {client} said: {message}\n";
                break;
            case ChatroomEvent.disconnect:
                chatroom.text += $" [{DateTime.Now:HH:mm:ss}] {client} was disconnected.\n";
                break;
            default:
                break;
        }
    }
}
