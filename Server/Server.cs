﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;

namespace Server
{
    class Server
    {
        static TcpListener portListener;
        static HashSet<TcpClient> setTCPClient = new HashSet<TcpClient>();
        static Dictionary<TcpClient, string> tcpClientNames = new Dictionary<TcpClient, string>();
        static TcpClient[] tcpClients = null;

        static void Main(string[] args)
        {
            portBind(4099);
            Console.WriteLine("Server started.");
            portAccept();
        }
        static void portBind(int portNumber)
        {
            portListener = new TcpListener(IPAddress.Any, portNumber);
            portListener.Start();
        }
        static void portAccept()
        {
            System.Threading.Thread tcpThread = new System.Threading.Thread(ClientLinking);
            tcpThread.Start();
            while (true)
            {
                TcpClient tcpClient = portListener.AcceptTcpClient();
                Console.WriteLine($"A client was connected.");
                lock (setTCPClient) setTCPClient.Add(tcpClient);
            }
        }
        static void ClientLinking()
        {
            while (true)
            {
                if (setTCPClient.Count == 0) continue;
                TcpClient[] tcpClientList;
                lock (setTCPClient)
                {
                    tcpClientList = new TcpClient[setTCPClient.Count];
                    setTCPClient.CopyTo(tcpClientList);
                }
                foreach (TcpClient client in tcpClientList)
                {
                    bool removeClient = false;
                    if (ClientConnected(client) == false)
                    {
                        removeClient = true;
                    }
                    else
                    {
                        try
                        {
                            if (client.Available > 0) //接收Client資料
                            {
                                ReceiveClientMessage(client);
                            }
                        }
                        catch (Exception)
                        {
                            removeClient = true;
                        }
                    }
                    if (removeClient)
                    {
                        lock (setTCPClient)
                        {
                            if (setTCPClient.Contains(client))
                            {
                                Console.WriteLine($"Client {tcpClientNames[client]} was disconnected.");
                                Remove(client);
                                setTCPClient.Remove(client);
                                tcpClientNames.Remove(client);
                            }
                        }
                    }
                }
            }
        }
        static bool ClientConnected(TcpClient client)
        {
            if (client.Connected == false) return false;
            try
            {
                if (client.Client.Poll(0, SelectMode.SelectRead))
                {
                    byte[] buffer = new byte[1];
                    if (client.Client.Receive(buffer, SocketFlags.Peek) == 0) return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
        static void ReceiveClientMessage(TcpClient client)
        {
            int iNumBytes = client.Available;
            byte[] buffer = new byte[iNumBytes]; //將接收到的字元存在Buffer
            client.GetStream().Read(buffer, 0, iNumBytes);
            string request = Encoding.UTF8.GetString(buffer)/*.Substring(0, iBytesRead)*/; //.Substring(0, iBytesRead)不一定需要
            if (request.StartsWith("Name:", StringComparison.OrdinalIgnoreCase)) //辨識Client傳入的訊息是否帶有Name:開頭
            {
                string[] tokens = request.Split(':');
                tcpClientNames.Add(client, tokens[1]);
                Console.WriteLine($"Client name: {tokens[1]}");
                //List(client);
                Add(client);
            }
            else if (request.StartsWith("Send:", StringComparison.OrdinalIgnoreCase)) //辨識Client傳入的訊息是否帶有Send:開頭
            {
                string[] tokens = request.Split(':');
                string message = tokens[1];
                lock (setTCPClient)
                {
                    tcpClients = new TcpClient[setTCPClient.Count];
                    setTCPClient.CopyTo(tcpClients);
                }
                foreach (TcpClient clientInList in tcpClients)
                {
                    //if (client != clientInList) //傳送訊息時忽略傳入的Client端
                    //{
                    string messageRequest = $"Message:{tcpClientNames[client]}:{message}";
                    byte[] requestBuffer = Encoding.UTF8.GetBytes(messageRequest);
                    clientInList.GetStream().Write(requestBuffer, 0, requestBuffer.Length);
                    //}
                }
            }
        }
        static void Remove(TcpClient client)
        {
            lock (setTCPClient)
            {
                tcpClients = new TcpClient[setTCPClient.Count];
                setTCPClient.CopyTo(tcpClients);
            }
            foreach (TcpClient clientInList in tcpClients)
            {
                if (client != clientInList) //傳送訊息時忽略傳入的Client端
                {
                    string messageRequest = $"Remove:{tcpClientNames[client]}";
                    byte[] requestBuffer = Encoding.UTF8.GetBytes(messageRequest);
                    clientInList.GetStream().Write(requestBuffer, 0, requestBuffer.Length);
                }
            }
        }
        static void Add(TcpClient client)
        {
            lock (setTCPClient)
            {
                tcpClients = new TcpClient[setTCPClient.Count];
                setTCPClient.CopyTo(tcpClients);
            }
            foreach (TcpClient clientInList in tcpClients)
            {
                if (client != clientInList) //傳送訊息時忽略傳入的Client端
                {
                    //對除了傳入的Client端以外的Client端通知有新加入的Client端
                    string messageRequest = $"Add:{tcpClientNames[client]}";
                    byte[] requestBuffer = Encoding.UTF8.GetBytes(messageRequest);
                    clientInList.GetStream().Write(requestBuffer, 0, requestBuffer.Length);
                }
                else
                {
                    //傳送Client List給新加入的Client端
                    string messageRequest = $"List:{tcpClientNames.Count}";
                    foreach (string s in tcpClientNames.Values)
                    {
                        messageRequest += $":{s}";
                    }
                    byte[] requestBuffer = Encoding.UTF8.GetBytes(messageRequest);
                    client.GetStream().Write(requestBuffer, 0, requestBuffer.Length);
                }
            }
        }
        //static void List(TcpClient client)
        //{
        //    lock (setTCPClient)
        //    {
        //        tcpClients = new TcpClient[setTCPClient.Count];
        //        setTCPClient.CopyTo(tcpClients);
        //    }
        //    string messageRequest = $"List:{tcpClientNames.Count}";
        //    foreach (string s in tcpClientNames.Values)
        //    {
        //        messageRequest += $":{s}";
        //    }
        //    byte[] requestBuffer = Encoding.UTF8.GetBytes(messageRequest);
        //    client.GetStream().Write(requestBuffer, 0, requestBuffer.Length);
        //}
    }
}
