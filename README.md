# Chatroom Project
## Chatroom scene

- Use TCP socket.
- Show local time when player sending message.
- Show any player connected or disconnected.
- Add, remove, sort and list all clients in player list.
