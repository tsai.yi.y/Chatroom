﻿using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class MessageEvent : MonoBehaviour
{
    private InputField message;
    public ClientManager clientManager;
    private void Awake()
    {
        message = GetComponent<InputField>();
    }
    public void OnSendMessage()
    {
        if (message.text != "")
        {
            clientManager.SendMessages(message.text);
            message.text = "";
        }
    }
}
