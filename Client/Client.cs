﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;

namespace Client
{
    class Client
    {
        static TcpClient client = null;

        static void Main(string[] args)
        {
            client = new TcpClient();
            Console.Write("Please enter a name: ");
            string clientName = Console.ReadLine();
            if (Connect("127.0.0.1", 4099))
            {
                SendName(clientName);
                while (true)
                {
                    while (Console.KeyAvailable == false)
                    {
                        if (client.Available > 0)
                        {
                            HandleReceiveMessages(client);
                        }
                        System.Threading.Thread.Sleep(100);
                    }
                    SendBroadcast(Console.ReadLine());
                }
            }
        }
        static bool Connect(string address, int port)
        {
            try
            {
                IPHostEntry host = Dns.GetHostEntry(address);
                IPAddress ipAddress = null;
                foreach (IPAddress h in host.AddressList)
                {
                    if (h.AddressFamily == AddressFamily.InterNetwork)
                    {
                        ipAddress = h;
                        break;
                    }
                }
                client.Connect(ipAddress.ToString(), port);
                Console.WriteLine($"Connected to Chat Server: {address}:{port}");
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine($"Exception happened: {e}");
                return false;
            }
        }
        static void SendName(string clientName)
        {
            string request = "Name:" + clientName;
            byte[] requestBuffer = Encoding.UTF8.GetBytes(request);
            client.GetStream().Write(requestBuffer, 0, requestBuffer.Length);
        }
        static void SendBroadcast(string clientMessage)
        {
            string request = "Send:" + clientMessage;
            byte[] requestBuffer = Encoding.UTF8.GetBytes(request);
            client.GetStream().Write(requestBuffer, 0, requestBuffer.Length);
        }
        static void HandleReceiveMessages(TcpClient client)
        {
            int iNumBytes = client.Available;
            byte[] buffer = new byte[iNumBytes];
            client.GetStream().Read(buffer, 0, iNumBytes);
            string request = Encoding.UTF8.GetString(buffer);
            if (request.StartsWith("Message:", StringComparison.OrdinalIgnoreCase))
            {
                Console.WriteLine($"[{DateTime.Now:HH:mm:ss}] {request.Split(':')[1]} said: {request.Split(':')[2]}");
            }
        }
    }
}
