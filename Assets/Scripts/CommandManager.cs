﻿using static ChatroomManager;
using static PlayerListManager;
using static ScrollbarValue;

public class CommandManager
{
    private ChatroomManager chatroomManager = InstanceChatroomManager();
    private PlayerListManager playerListManager = InstancePlayerListManager();
    private ScrollbarValue scrollbarValue = InstanceScrollbarValue();
    public void ReceivedCommands(string command)
    {
        switch (command.Split(':')[0])
        {
            case "Message":
                chatroomManager.RefreshChatroom(ChatroomEvent.message, command.Split(':')[1], command.Split(':')[2]);
                break;
            case "List":
                playerListManager.RefreshPlayerList(ListEvent.build, command);
                break;
            case "Add":
                chatroomManager.RefreshChatroom(ChatroomEvent.connect, command.Split(':')[1]);
                playerListManager.RefreshPlayerList(ListEvent.add, command.Split(':')[1]);
                break;
            case "Remove":
                chatroomManager.RefreshChatroom(ChatroomEvent.disconnect, command.Split(':')[1]);
                playerListManager.RefreshPlayerList(ListEvent.remove, command.Split(':')[1]);
                break;
            default:
                break;
        }
        scrollbarValue.ResetScrollbarValue();
    }
}
