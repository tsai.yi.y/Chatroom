﻿using UnityEngine;
using UnityEngine.UI;

public class ScrollbarValue : MonoBehaviour
{
    private Scrollbar scrollbar; 
    
    private static ScrollbarValue instance;
    public static ScrollbarValue InstanceScrollbarValue() { return instance; }

    private void Awake()
    {
        instance = this;
        scrollbar = GetComponent<Scrollbar>();
    }
    public void ResetScrollbarValue() => scrollbar.value = 0;
}
